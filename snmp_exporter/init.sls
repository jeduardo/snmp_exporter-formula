{% from "snmp_exporter/map.jinja" import settings %}
snmp_user:
  user.present:
    - name: {{ settings.system_username }}
    - shell: /sbin/nologin
    - system: True

snmp_binary:
  archive.extracted:
    - name: {{ settings.dist_dir }}
    - source: {{ settings.download_url }}
    - source_hash: {{ settings.archive_hash }}
    - keep: True

snmp_create_symlink:
  file.symlink:
    - name: /usr/local/bin/snmp-exporter
    - target: {{ settings.dist_dir }}/snmp_exporter-{{ settings.version }}.linux-amd64/snmp_exporter

snmp_deploy_config:
  file.managed:
    - name: {{ settings.config_location }}
    - contents: |
        {{ settings.config|yaml(False)|string|indent(8) }}

snmp_write_unitfile:
  file.managed:
    - name: /etc/systemd/system/snmp-exporter.service
    - contents: |
        [Unit]
        After=network-online.target
        Requires=network-online.target

        [Service]
        User={{ settings.system_username }}
        Restart=on-failure
        ExecStart=/usr/local/bin/snmp-exporter \
          --config.file={{ settings.config_location }}

        [Install]
        WantedBy=multi-user.target
snmp_ensure_service:
  service:
    - running
    - enable: true
    - name: snmp-exporter.service
    - watch:
      - file: snmp_write_unitfile
      - file: snmp_deploy_config
